import fetch from "node-fetch";

let response;

try
{
  response = await fetch("https://trigger-bitbucket-pipeline-ffpyydop4a-ey.a.run.app/trigger-build", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      projectt: "2",
    })
  })
}
catch (e)
{
  console.error("Error: " + e);
}

if(!response.ok)
{
  console.error("Error: " + response.status + " - " + response.statusText);
}

try
{
  response = await response.json();
}
catch (e)
{
  console.error("Error: " + e);
}

if(response.error)
{
  console.error(response.error);
}
else
{
  console.log(response);
}


