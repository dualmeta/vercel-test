#!/bin/bash

deploy()
{
  prodArgument=""

  if [[ $BITBUCKET_BRANCH == "main" ]]; then prodArgument="--prod"; fi

  if [[ $1 == "" ]]; then
    echo "Error: Vercel name for project is not defined. Please check again if it was defined in your environment variables."
    exit 1
  fi

  if [[ $VERCEL_SCOPE == "" ]]; then
    echo "Error: Vercel scope is not defined. Please check again if it was defined in your environment variables."
    exit 1
  fi

  if [[ $VERCEL_TOKEN == "" ]]; then
    echo "Error: Vercel token is not defined. Please check again if it was defined in your environment variables."
    exit 1
  fi

  rm -rf .vercel
  vercel "$prodArgument" --token="$VERCEL_TOKEN" --scope="$VERCEL_SCOPE" --name="$1" --confirm --no-clipboard
  rm -rf .vercel
}

didApp1Change=0
didApp2Change=0

git diff HEAD^ HEAD --quiet ./app1/ || didApp1Change=1
git diff HEAD^ HEAD --quiet ./app2/ || didApp2Change=1

if [ $didApp1Change == 1 ] || [ $didApp2Change == 1 ]; then
  yarn global add vercel
else
  echo "No changes detected. Exit."
  exit 0;
fi

if [[ $didApp1Change == 1 ]]; then
  echo -e "\ndeploy awesome app 1"
  deploy "$VERCEL_NAME_AWESOME_APP_1"
else
  echo "No code changes in app1. No deploy needed."
fi

if [[ $didApp2Change == 1 ]]; then
  echo -e "\ndeploy awesome app 2"
  deploy "$VERCEL_NAME_AWESOME_APP_2"
else
  echo "No code changes in app2. No deploy needed."
fi
