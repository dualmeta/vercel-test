#!/bin/bash

requestedProject="$PROJECT"
requestedProjectNameIsValid=1
requestedProjectVercelName=""

awesomeApp1="awesome app 1"
awesomeApp2="awesome app 2"

if [[ $requestedProject == "" ]]; then
  echo "Error: No project was defined. Please check variables of Bitbucket API Call."
  exit 1
fi

echo "Deploy of project '$requestedProject' requested"

declare -a apps=(
  "$awesomeApp1"
  "$awesomeApp2"
)

for (( i=0; i<${#apps[@]}; i++ )); do
  if [[ ${apps[$i]} == "$requestedProject" ]]; then
    requestedProjectNameIsValid=0
    break
  fi
done

if [[ $requestedProjectNameIsValid == 1 ]]; then
  echo "Error: Requested project does not exist"
  exit 1
fi

case $requestedProject in
  "$awesomeApp1")
    requestedProjectVercelName="$VERCEL_NAME_AWESOME_APP_1"
    ;;
  "$awesomeApp2")
    requestedProjectVercelName="$VERCEL_NAME_AWESOME_APP_2"
    ;;
esac

if [[ $requestedProjectVercelName == "" ]]; then
  echo "Error: Vercel name for project is not defined. Please check again if it was defined in your environment variables."
  exit 1
fi

if [[ $VERCEL_SCOPE == "" ]]; then
  echo "Error: Vercel scope is not defined. Please check again if it was defined in your environment variables."
  exit 1
fi

if [[ $VERCEL_TOKEN == "" ]]; then
  echo "Error: Vercel token is not defined. Please check again if it was defined in your environment variables."
  exit 1
fi

yarn global add vercel
rm -rf .vercel
echo "deploy app '$requestedProjectVercelName'"
vercel --prod --token="$VERCEL_TOKEN" --scope="$VERCEL_SCOPE" --name="$requestedProject" --confirm --no-clipboard
rm -rf .vercel
